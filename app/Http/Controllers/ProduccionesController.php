<?php

namespace App\Http\Controllers;

use App\Models\Produccion;
use Illuminate\Http\Request;

class ProduccionesController extends Controller
{
    //
    public function index()
    {

        $$producciones = Produccion::get();
        return view('producciones.index', ['producciones' => $producciones]);
    }


    public function create()
    {
        return view('producciones.crear');
    }


    public function store(Request $request)
    {
        Produccion::create($request->all());
        return redirect()->route('producciones.index')->with('success', 'Producción creada correctamente');
    }


    public function show(Produccion $produccion)
    {
        return view('producciones.show', compact('produccion'));
    }

    


}
