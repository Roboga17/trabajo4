<?php

namespace App\Http\Controllers;

use App\Models\Ingrediente;
use App\Models\Insumo;
use App\Models\Producto;

use Illuminate\Http\Request;

class IngredientesController extends Controller
{
    public function index()
    {
        $ingredientes = Ingrediente::with(['insumo', 'producto'])->get();

            // Pasar las variables a la vista

        return view('ingredientes.listar', compact('ingredientes'));


        } 
        
        public function ingredientesPorProducto($producto_id)
        {
            $ingredientes = Ingrediente::with(['insumo', 'producto'])
                ->where('producto_id', $producto_id)
                ->get();
        
            return view('ingredientes.listar', compact('ingredientes'));
        }
        

    public function create()
    {
        
        return view('ingredientes.crear');
    }

    public function store(Request $request)
    {
        
        //dd($request->all());
        $ingrediente = new Ingrediente();//crea un nuevo registro en la BD
        $ingrediente->producto_id = $request->producto_id;
        $ingrediente->insumo_id = $request->insumo_id;
        $ingrediente->cantidad = $request->cantidad;
        $ingrediente->save();


        return redirect()->route('ingredientes')
            ->with('success', 'Insumo creado exitosamente.');
    }


}
