<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index(Request $request)
    {
        // Obtener todos los productos
        $productos = Producto::query();

        // Filtrar por nombre
        $nombre = $request->input('nombre');
        if ($nombre) {
            $productos = $productos->where('nombre', 'ilike', "%$nombre%");
        }

        // Filtrar por estado (activo/inactivo)
        $activo = $request->input('activo');
        if ($activo !== null) {
            $productos = $productos->where('activo', $activo);
        }

        // Obtener resultados
        $productos = $productos->get();

        return view('productos.listar', [
            'productos' => $productos,
        ]);
    }



    public function create()
    {
        return view('productos.crear');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $producto = new Producto();//crea un nuevo registro en la BD
        $producto->nombre = $request->nombre;
        $producto->codigo = $request->codigo;
        $producto->cantidad = $request->cantidad;
        $producto->activo = ($request->activo == 'si' ? true : false);
        $producto->save();


        return redirect()->route('productos')
            ->with('success', 'Producto creado exitosamente.');
    }

    





}
