<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->scopes(['email'])->redirect();
        
    }

    public function callback(Request $request)
    {
        $this->middleware('auth');
        try {
            // Obtén los datos del usuario desde Facebook
            $facebookUser = Socialite::driver('facebook')->user();
            //dd($facebookUser);


            // Busca si el usuario ya existe en la base de datos
            $findUser = User::where('facebook_id', $facebookUser->id)->first();

            if ($findUser) {
                // El usuario ya existe, autentícalo
                Auth::login($findUser);
                return redirect()->intended('/');
            } else {
                // Crea un nuevo usuario en la base de datos
                $newUser = User::create([
                    'name' => $facebookUser->name,
                    'email' => $facebookUser->email,
                    'facebook_id' => $facebookUser->id,
                    //'facebook_access_token' => $facebookUser->token,
                ]);

                // Autentica al nuevo usuario
                Auth::login($newUser);
                return redirect()->intended('/');
            }
        } catch (Exception $e) {
            // Log de la excepción para referencia futura
            Log::error($e);

            // Redirige al usuario a la página de inicio con un mensaje de error genérico
            return redirect('/')->with('error', 'Error durante el proceso de inicio de sesión con Facebook. Por favor, inténtelo de nuevo.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function showLoginForm()
    {
        return view('loginFace');
    }

}
