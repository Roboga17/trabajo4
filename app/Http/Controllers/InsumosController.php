<?php

namespace App\Http\Controllers;

use App\Models\Insumo;
use Illuminate\Http\Request;
use App\Models\Ingrediente;


class InsumosController extends Controller
{
    //

    public function index(Request $request)
    {
        // Obtener todos los insumos
        $insumos = Insumo::all();
    
        $nombre = $request->input('nombre');
        $insumosPorNombre = ($nombre) ? Insumo::where('nombre', 'like', "%$nombre%")->get() : null;
        
        if ($nombre) {
            $insumos = $insumos->filter(function ($insumo) use ($nombre) {
                return $insumo->nombre == $nombre;
            });
        }

        $activo = $request->input('activo');
        if ($activo !== null) {
            $insumos = $insumos->where('activo', $activo);
        }
    
        // Verificar si se ha enviado el formulario de búsqueda de insumos sin ingredientes
        if ($request->filled('ingrediente')) {
            // Obtener los IDs de insumos que tienen ingredientes
            $insumosConIngredientes = Ingrediente::distinct('insumo_id')->pluck('insumo_id');
    
            // Obtener los insumos que NO tienen ingredientes
            $insumos = $insumos->reject(function ($insumo) use ($insumosConIngredientes) {
                return in_array($insumo->id, $insumosConIngredientes->toArray());
            });
        }
    
        return view('insumos.listar', [
            'insumos' => $insumos,
        ]);
    }


    
    

}








