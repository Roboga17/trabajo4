<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'productos';


    protected $fillable = ['nombre', 'codigo', 'activo', 'cantidad'];


    public function ingredientes()
    {
        return $this->hasMany(Ingrediente::class, 'producto_id');
    }

    public function producciones()
    {
        return $this->hasMany(Produccion::class, 'producto_id');
    }


}
