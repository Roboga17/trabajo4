<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produccion extends Model
{
    use HasFactory;
    protected $table='producciones';

    protected $fillable = [
        'fecha',
        'operador',
        'cantidad_a_producir',
        'producido',
        'producto_id',
    ];

    
    public function producto()
    {
        return $this->belongsTo(Producto::class,'producto_id','id');
    }



}
