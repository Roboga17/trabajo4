<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insumo extends Model
{
    use HasFactory;

    protected $table = 'insumos';


    protected $fillable = ['nombre', 'codigo', 'activo'];

    public function ingredientes()
    {
        return $this->hasMany(Ingrediente::class, 'insumo_id');
    }

}

