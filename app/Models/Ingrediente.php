<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Ingrediente extends Model
{
    use HasFactory;

    protected $table = 'ingredientes';

    protected $fillable = ['producto_id', 'insumo_id', 'cantidad'];

    
    public function producto()
    {
        return $this->belongsTo(Producto::class, 'producto_id','id');
    }

    public function insumo()
    {
        return $this->belongsTo(Insumo::class, 'insumo_id','id');
    }


}
