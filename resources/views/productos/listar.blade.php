<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado Productos</title>
    <link rel="stylesheet" href="{{ asset('css/bulma0.9.4.min.css') }}">
</head>
<body>
    <section class="section">

        
                @auth
                <p class="content">Bienvenido, {{ Auth::user()->name }} | Email: {{ Auth::user()->email }}  </p>
                @else
                <p class="content">No estás autenticado.</p>
                @endauth
        <h1 class="title has-text-weight-bold mb-12">Listado de Productos</h1>

        <h4 class="title has-text-medium-bold">Búsqueda de Productos existentes</h4>

        <form action="{{ route('productos') }}" method="GET">
            <!-- Formulario para buscar por Nombre -->
            <div class="field">
                <label class="label">Buscar por Nombre:</label>
                <div class="control">
                    <input class="input" type="text" name="nombre" value="{{ request('nombre') }}" placeholder="Nombre" autofocus size="20">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Buscar por Nombre</button>
                </div>
            </div>
        </form>

        <form action="{{ route('productos') }}" method="GET">
            <!-- Formulario para buscar por Activo -->
            <div class="field">
                <label class="label">Buscar por Activo:</label>
                <div class="control">
                    <div class="select">
                        <select name="activo">
                            <option value="">Todos</option>
                            <option value="1" {{ request('activo') == '1' ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ request('activo') == '0' ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Buscar </button>
                </div>
            </div>
        </form>

        <br><br>

        <!-- Mostrar tabla de productos -->
        @if ($productos->isNotEmpty())
            <table class="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Codigo</th>
                        <th>Activo</th>
                        <th>Cantidad</th>
                        <th>Ingredientes</th>
                        <th>Producciones</th>
                        <th>Enlace Producto</th>
                    </tr>
                </thead>
                <tbody>

                    @forelse($productos as $producto)
                        <tr>
                            <td>{{ $producto->nombre }}</td>
                            <td>{{ $producto->codigo }}</td>
                            <td>
                                <span class="tag {{ $producto->activo ? 'is-success' : 'is-danger' }}">
                                    {{ $producto->activo ? 'Si' : 'No' }}
                                </span>
                            </td>
                            <td>{{ $producto->cantidad }}</td>
                            <td>{{ $producto->ingredientes->count() }}</td>
                            <td>{{ $producto->producciones->count() }}</td>
                            <td>
                                <a href="{{ route('ingredientes-producto', ['producto_id' => $producto->id]) }}">
                                    Detalles Producto
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">No se encontraron resultados.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        @else
            <h2 class="subtitle has-text-warning has-text-weight-bold mb-4">
                No se encontraron resultados.
            </h2>
        @endif

        @if ($productos->isNotEmpty() && (count($productos) == 0))
            <h2 class="subtitle has-text-warning has-text-weight-bold mb-4">
                No hay productos creados. ¿Quieres crear un nuevo producto?
                <a href="{{ route('productos-crear') }}">aca</a>
            </h2>
        @endif

        <br>
        <br>
        <a href="{{ route('productos-crear') }}" class="button is-info">Crear Nuevo Producto</a>

        <br>
        <br>
        <a href="{{ route('welcome') }}" class="button is-info">Volver al Menú</a>
    </section>
</body>
</html>


