<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bienvenida</title>
    <link rel="stylesheet" href="{{ asset('css/bulma0.9.4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style>
        body {
            background-color: #f4f4f4;
        }

        /* Aplica un estilo personalizado a la clase .small-hero-body */
        .small-hero-body {
            max-height: 100px; /* Ajusta la altura máxima según tus necesidades */
        }

        .hero-body {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .container {
            text-align: center;
        }

        .content {
            font-size: 1.2rem;
            color: #333;
            margin-bottom: 20px;
        }

        .button {
          background-color: #ffffff;;
            color: black;
        }

        .button:hover {
            background-color: #225aba;
        }

        .title {
            font-size: 2rem;
            color: #000;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>

    <section class="hero is-medium is-info">
        <div class="hero-body small-hero-body">
            <div class="container">
                @auth
                <p class="content">Bienvenido, {{ Auth::user()->name }} | Email: {{ Auth::user()->email }}  </p>
                @else
                <p class="content">No estás autenticado.</p>
                @endauth

                <a href="{{ route('auth-redirect')}}" class="button is-small">
                    Login con Facebook
                </a>    <a href="{{ route('logout') }}" class="button is-small">
                      Cerrar Sesión
                  </a>

                

                <h1 class="title">Bienvenidos al sitio PRODUCCION</h1>
                <h1 class="title">Nombre: Roberto Bogado</h1>
            </div>
        </div>
    </section>


    <section class="section">
    <div class="container text-center">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card bg-info text-white">
                    <div class="card-body">
                        <h5 class="card-title font-weight-bold mb-3">DER PRODUCCION</h5>
                        <img src="imagen/t4-bogado.png" alt="DER" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section">
  <div class="container">
    <div class="columns is-centered is-vcentered">
      <div class="column is-one-third has-text-centered">
        <div class="box has-background-success">
          <h1 class="title is-4 has-text-white">Enlaces a páginas de PRODUCCIÓN</h1>
          <hr class="has-background-white">
          <a href="{{ route('insumos') }}" class="button is-secondary is-fullwidth">Pagina 1 - Insumos</a><br>

          <a href="{{ route('productos') }}" class="button is-secondary is-fullwidth">Pagina 2 - Productos</a><br>

          <a href="{{ route('ingredientes')}}" class="button is-secondary is-fullwidth">Pagina 3 - Ingredientes</a>


          
        </div>
      </div>
    </div>
  </div>


</section>



</body>

</html>



        