<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listar Insumos</title>
    <link rel="stylesheet" href="{{ asset('css/bulma0.9.4.min.css') }}">
</head>
<body>
    <section class="section">

        
                @auth
                <p class="content">Bienvenido, {{ Auth::user()->name }} | Email: {{ Auth::user()->email }}  </p>
                @else
                <p class="content">No estás autenticado.</p>
                @endauth

        <h1 class="title has-text-weight-bold mb-12">Insumos</h1>

        <h4 class="title has-text-medium-bold">Búsqueda de Insumos existentes</h4>

        <form action="{{ route('insumos') }}" method="GET">
            <!-- Formulario para buscar por Nombre -->
            <div class="field">
                <label class="label">Buscar por Nombre:</label>
                <div class="control">
                    <input class="input" type="text" name="nombre" value="{{ request('nombre') }}" placeholder="Nombre" autofocus size="20">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Buscar por Nombre</button>
                </div>
            </div>
        </form>

        <form action="{{ route('insumos') }}" method="GET">
            <!-- Formulario para buscar por Activo -->
            <div class="field">
                <label class="label">Buscar por Activo:</label>
                <div class="control">
                    <div class="select">
                        <select name="activo">
                            <option value="">Todos</option>
                            <option value="1" {{ request('activo') == '1' ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ request('activo') == '0' ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Buscar</button>
                </div>
            </div>
        </form>

        <br><br>

        <h2 class="subtitle has-text-weight-bold">Insumos sin Ingredientes</h2>

        <form action="{{ route('insumos') }}" method="GET">
            <!-- Formulario para buscar insumos sin ingredientes -->
            <div class="field">
                <div class="control">
                    <button type="submit" name="ingrediente" value="1" class="button is-info">Buscar Insumos sin Ingredientes</button>
                </div>
            </div>
        </form>

        <!-- Mostrar tabla de insumos -->
        @if ($insumos->isNotEmpty())
            <table class="table is-fullwidth is-hoverable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Codigo</th>
                        <th>Activo</th>
                        <th>Ingredientes</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($insumos as $insumo)
                        <tr>
                            <td>{{ $insumo->nombre }}</td>
                            <td>{{ $insumo->codigo }}</td>
                            <td>
                                <span class="tag {{ $insumo->activo ? 'is-success' : 'is-danger' }}">
                                    {{ $insumo->activo ? 'Si' : 'No' }}
                                </span>
                            </td>
                            <td>{{ $insumo->ingredientes->count() }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">No se encontraron resultados.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        @else
            <h2 class="subtitle has-text-warning has-text-weight-bold mb-4">
                @if (request()->filled('ingrediente'))
                    No se encontraron insumos sin ingredientes.
                @else
                    No se encontraron resultados.
                @endif
            </h2>
        
        @endif

        @if ($insumos->isNotEmpty() && (count($insumos) == 0))
            <h2 class="subtitle has-text-warning has-text-weight-bold mb-4">
                No hay insumos creados. ¿Quieres crear un nuevo insumo?
                <a href="{{ route('insumos-crear') }}">aca</a>
            </h2>
        @endif

        <br>
        <br>
        <a href="{{ route('insumos-crear') }}" class="button is-info">Crear Nuevo Insumos<a>

        <br>
        <br>
        <a href="{{ route('welcome') }}" class="button is-info">Volver al Menú</a>

    </section>
</body>
</html>



