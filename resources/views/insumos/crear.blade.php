<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

   <link rel="stylesheet" href="{{asset('css/bulma0.9.4.min.css')}}">

    <title>Crear Insumos</title>
</head>
<body>
       <section class="section">


                        <h1 class="title has-text-weight-bold mb-12">Crear Insumos </h1>

                            <form method="POST" action="{{ route('insumos-store') }}">
                              @csrf
                          <div class="field">
                            <label class="label">Nombre</label>
                            <div class="control">
                              <input class="input" type="text" placeholder="Nombre" autofocus size="25" name="nombre" required>
                            </div>
                          </div>


                          <div class="field">
                            <label class="label">Codigo</label>
                            <div class="control">
                            <input class="input" type="text" placeholder="Codigo" autofocus size="25" name="codigo" required>
                              </div>
                            </div>
                          </div>


                          </div>


                          <div class="field">
                            <div class="control">
                              <label class="radio">
                                <p>Activo</p>
                                <input type="radio" name="activo" value="si" required>
                                Si
                              </label>
                              <label class="radio">
                                <input type="radio" name="activo" value="no" required>
                                No
                              </label>
                            </div>
                          </div>

                                    <input type="submit" value="Enviar" />
                                    <input type="reset" value="Cancelar" />

                        </form>
                        <br>
                       
                         <a href="{{ route('welcome') }}">Volver al Inicio</a>

      </section>

</body>

</html>