<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/bulma0.9.4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">

</head>
<body>

<section class="section">
        <div class="container">


            <h2 class="title">Favor registrarse para continuar visualizando las paginas</h2>

                <form method="POST" action="{{ route('login') }}" class="form">
                    @csrf

                    <div class="field">
                        <label for="email" class="label">Email:</label>
                        <div class="control">
                            <input type="email" name="email" class="input" placeholder="Ingrese email" required>
                        </div>
                    </div>


                    <div class="field">
                        <a href="{{ route('auth-redirect')}}" class="button is-primary is-fullwidth">
                            <span>Login con Facebook</span>
                            <i class="fab fa-facebook"></i>
                        </a>
                    </div>
                </form>
    </div>
</section>


</body>
</html>
