<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listado de Ingredientes</title>
    <link rel="stylesheet" href="{{ asset('css/bulma0.9.4.min.css') }}">
</head>
<body>
    <section class="section">
        <div class="container">

            
                @auth
                <p class="content">Bienvenido, {{ Auth::user()->name }} | Email: {{ Auth::user()->email }}  </p>
                @else
                <p class="content">No estás autenticado.</p>
                @endauth

            <h1 class="title">Listado de Ingredientes</h1>

            @if ($ingredientes->isEmpty())
                <p>No hay registros disponibles.</p>
                No hay ingredientes creados. ¿Quieres crear un nuevo ingrediente?
                <a href="{{ route('ingredientes-crear') }}">aca</a>
            @else
                @foreach($ingredientes->groupBy('producto_id') as $productoId => $ingredientesPorProducto)
                    <h2 class="subtitle has-text-weight-bold has-text-danger">{{ $ingredientesPorProducto->first()->producto->nombre }}</h2>

                    <table class="table is-fullwidth">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Insumo_id</th>
                                <th>Nombre_insumo</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ingredientesPorProducto as $registro)
                                <tr>
                                    <td>{{ $registro->id }}</td>
                                    <td>{{ $registro->insumo_id }}</td>
                                    <td>{{ $registro->insumo->nombre }}</td>
                                    <td>{{ $registro->cantidad }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <br>
                @endforeach

                <a href="{{ route('ingredientes-crear') }}" class="button is-info">Crear Nuevo Ingrediente</a>
            @endif
        </div>
        <br>

        <a href="{{ route('welcome') }}" class="button is-info">Volver al Menú</a>
    </section>
</body>
</html>





