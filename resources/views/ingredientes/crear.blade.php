<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

   <link rel="stylesheet" href="{{asset('css/bulma0.9.4.min.css')}}">

    <title>Crear Ingredientes</title>
</head>
<body>
       <section class="section">


                        <h1 class="title has-text-weight-bold mb-12">Crear Ingredientes </h1>

                            <form method="POST" action="{{ route('ingredientes-store') }}">
                              @csrf
                          <div class="field">
                            <label class="label">Producto_id</label>
                            <div class="control">
                              <input class="input" type="number" placeholder="Producto_id" autofocus size="25" name="producto_id" required>
                            </div>
                          </div>


                          <div class="field">
                            <label class="label">Insumo_id</label>
                            <div class="control">
                            <input class="input" type="number" placeholder="Insumo_id" autofocus size="25" name="insumo_id" required>
                              </div>
                            </div>
                          </div>

                          <div class="field">
                            <label class="label">Cantidad</label>
                            <div class="control">
                            <input class="input" type="number" placeholder="Cantidad" autofocus size="25" name="cantidad" required>
                              </div>
                            </div>
                          </div>




                                    <input type="submit" value="Enviar" />
                                    <input type="reset" value="Cancelar" />

                        </form>
                        <br>
                       
                        <a href="{{ route('welcome') }}">Volver al Inicio</a>

      </section>

</body>

</html>