<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\InsumosController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\IngredientesController;
use App\Http\Controllers\ProduccionesController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {   return view('welcome');});


Route::get('/', function () {return view('welcome');})->name('welcome');

//Ruta para Insumos

Route::get('/insumos', [InsumosController::class, 'index'])->name('insumos');

Route::get('/insumos/sin-ingredientes', [InsumosController::class, 'insumosSinIngredientes'])->name('insumos-sin-ingredientes');

Route::get('/insumos/crear', [InsumosController::class, 'create'])->name('insumos-crear');

Route::post('/insumos/store', [InsumosController::class, 'store'])->name('insumos-store');

//Ruta para Productos

Route::get('/productos', [ProductosController::class, 'index'])->name('productos');
Route::get('/productos/crear', [ProductosController::class, 'create'])->name('productos-crear');
Route::post('/productos/store', [ProductosController::class, 'store'])->name('productos-store');

//Ruta para Ingredientes

Route::get('/ingredientes', [IngredientesController::class, 'index'])->name('ingredientes');

Route::get('/ingredientes/{producto_id}', [IngredientesController::class, 'ingredientesPorProducto'])->name('ingredientes-producto');


Route::get('/ingredientes/crear', [IngredientesController::class, 'create'])->name('ingredientes-crear');

Route::get('/ingredientes/store', [IngredientesController::class, 'store'])->name('ingredientes-store');

Route::post('/ingredientes/store', [IngredientesController::class, 'store'])->name('ingredientes-store');

//Ruta para Login

Route::get('/auth/facebook', [AuthController::class, 'redirect'])->name ('auth-redirect');
Route::get('/auth/facebook/callback', [AuthController::class, 'callback'])->name ('auth-callback');
Route::get('/login', [AuthController::class, 'showLoginForm'])->name ('login');
Route::get('/logout', [AuthController::class, 'logout'])->name ('logout');


// Rutas que requieren autenticación
Route::middleware(['auth'])->group(function () {

    Route::get('/insumos', [InsumosController::class, 'index'])->name('insumos');
    Route::get('/productos', [ProductosController::class, 'index'])->name('productos');
    Route::get('/ingredientes', [IngredientesController::class, 'index'])->name('ingredientes');
    
});
