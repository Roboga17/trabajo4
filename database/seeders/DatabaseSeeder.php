<?php

namespace Database\Seeders;

use App\Models\Insumo;
use App\Models\Producto;
use App\Models\Ingrediente;
use App\Models\Produccion;



use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
         //CARGAR DATOS EN LA TABLA Insumo
              $insumo = new Insumo();
                $insumo->nombre = "Harina";
                $insumo->codigo = "021";
                $insumo->activo = true;
                $insumo->save();

                $insumo = new Insumo();
                $insumo->nombre = "Azucar";
                $insumo->codigo = "022";
                $insumo->activo = true;
                $insumo->save();

                $insumo = new Insumo();
                $insumo->nombre = "Aceite";
                $insumo->codigo = "023";
                $insumo->activo = true;
                $insumo->save();

                $insumo = new Insumo();
                $insumo->nombre = "Arroz";
                $insumo->codigo = "024";
                $insumo->activo = true;
                $insumo->save();

                $insumo = new Insumo();
                $insumo->nombre = "Pan";
                $insumo->codigo = "025";
                $insumo->activo = false;
                $insumo->save();

                //Para crear productos
                $producto = new Producto();
                $producto->nombre = "Sanwinch";
                $producto->codigo = "120";
                $producto->activo = true;
                $producto->cantidad = 20;

                $producto->save();

                $producto = new Producto();
                $producto->nombre = "Guiso";
                $producto->codigo = "121";
                $producto->activo = true;
                $producto->cantidad = 10;

                $producto->save();

                $producto = new Producto();
                $producto->nombre = "Estofado";
                $producto->codigo = "122";
                $producto->activo = true;
                $producto->cantidad = 10;

                $producto->save();

                $producto = new Producto();
                $producto->nombre = "Bife";
                $producto->codigo = "123";
                $producto->activo = true;
                $producto->cantidad = 10;

                $producto->save();

                $producto = new Producto();
                $producto->nombre = "Tallarin";
                $producto->codigo = "123";
                $producto->activo = true;
                $producto->cantidad = 10;

                $producto->save();

               //Para crear ingredientes
                 $ingrediente = new Ingrediente();
                 $ingrediente->producto_id = "3";
                 $ingrediente->insumo_id = "3";
                 $ingrediente->cantidad = 35;
                 $ingrediente->save();

                 $ingrediente = new Ingrediente();
                 $ingrediente->producto_id = "1";
                 $ingrediente->insumo_id = "2";
                 $ingrediente->cantidad = 20;
                 $ingrediente->save();

                 $ingrediente = new Ingrediente();
                 $ingrediente->producto_id = "2";
                 $ingrediente->insumo_id = "1";
                 $ingrediente->cantidad = 10;
                 $ingrediente->save();

                 $ingrediente = new Ingrediente();
                 $ingrediente->producto_id = "4";
                 $ingrediente->insumo_id = "5";
                 $ingrediente->cantidad = 8;
                 $ingrediente->save();

                 $ingrediente = new Ingrediente();
                 $ingrediente->producto_id = "5";
                 $ingrediente->insumo_id = "5";
                 $ingrediente->cantidad = 12;
                 $ingrediente->save();

                //Para crear producciones
                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Roberto";
                  $produccion->cantidad_a_producir = 10;
                  $produccion->producido = true;
                  $produccion->producto_id="1";
                  $produccion->save();


                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Jose";
                  $produccion->cantidad_a_producir = 20;
                  $produccion->producido = true;
                  $produccion->producto_id="2";
                  $produccion->save();


                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Mario";
                  $produccion->cantidad_a_producir = 25;
                  $produccion->producido = true;
                  $produccion->producto_id="3";
                  $produccion->save();

                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Marcelo";
                  $produccion->cantidad_a_producir = 25;
                  $produccion->producido = true;
                  $produccion->producto_id="4";
                  $produccion->save();


                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Luis";
                  $produccion->cantidad_a_producir = 10;
                  $produccion->producido = true;
                  $produccion->producto_id="2";
                  $produccion->save();

                  $produccion = new Produccion();
                  $produccion->fecha = "2023-11-20 15:00:05";
                  $produccion->operador = "Milton";
                  $produccion->cantidad_a_producir = 10;
                  $produccion->producido = true;
                  $produccion->producto_id="1";
                  $produccion->save();

    }
}
