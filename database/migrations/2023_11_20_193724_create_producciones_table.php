<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producciones', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->string('operador', 60);
            $table->integer('cantidad_a_producir')->nullable();
            $table->boolean('producido')->comment('Indica si la producción ya se realizó o está planificada');
            $table->unsignedBigInteger('producto_id');
            $table->timestamps();

            // Definir la clave foránea
            $table->foreign('producto_id')->references('id')->on('productos');
        });
    }

    public function down()
    {
        Schema::dropIfExists('producciones');
    }
};