<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('producto_id');
            $table->unsignedBigInteger('insumo_id');
            $table->integer('cantidad');
            $table->timestamps();

            // Definir las claves foráneas
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('insumo_id')->references('id')->on('insumos');
        });
    }

    public function down()
    {
        Schema::dropIfExists('ingredientes');
    }
};