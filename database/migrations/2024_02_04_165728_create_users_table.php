<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Elimina la tabla 'users' si existe
        Schema::dropIfExists('users');

        // Crea la nueva tabla 'users' con las columnas necesarias
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('facebook_access_token')->nullable();
            $table->rememberToken();
            $table->timestamps();

            Schema::table('users', function (Blueprint $table) {
                $table->string('name', 255)->change();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Revierte la creación de la tabla 'users' en caso de hacer un rollback
        Schema::dropIfExists('users');
    }
};
